li $r0, 0   //coments
li $r1, 15
DISP: disp $r0, 0
disp $r1, 1
lw $r2, 0($r0)     
lw $r3, 0($r1)
bgt $r2, $r3, 2    
sw $r2, 0($r1)
sw $r3, 0($r0)        
addi $r1, $r1, -1   
bne $r0, $r1, DISP  
jump JMP
nop
or $r0, $r0, $r0
JMP: addi $r0, $r0, 1
li $r1, 15
bne $r0, $r1, DISP
halt


mv $r0, $r1  #macro instruction