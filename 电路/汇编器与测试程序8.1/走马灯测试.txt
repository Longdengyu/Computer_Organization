﻿li $r0, 1  
li $r1, 15
sllv $r3, $r0, $r1 
srav $r3, $r3, $r1   //$r3=0xFFFF
li $r0, 0
li $r1, 4
lui $r2, 12
srlv $r2, $r2, $r1
srlv $r2, $r2, $r1   //$r2=12

Loop:addi $r0, $r0, 1
andi $r0, $r0, 15

sllv $r3, $r3, $r1   //走马灯左移
or $r3, $r3, $r0
disp $r3, 0
sllv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0
sllv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0
sllv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0

addi $r0, $r0, 1
andi $r0, $r0, 15
sllv $r0, $r0, $r2   

srlv $r3, $r3, $r1   //走马灯右移
or $r3, $r3, $r0
disp $r3, 0
srlv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0
srlv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0
srlv $r3, $r3, $r1 
or $r3, $r3, $r0
disp $r3, 0

srlv $r0, $r0, $r2 

bne $r0, $r2, Loop
halt